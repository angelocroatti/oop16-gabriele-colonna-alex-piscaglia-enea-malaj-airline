package controller.interfaces;
/**
 * Add Destination Controller interface.
 */
public interface AddDestinationController {

    /**
     * Adds a destination to the list of destinations.
     */
    void addDestination();

}