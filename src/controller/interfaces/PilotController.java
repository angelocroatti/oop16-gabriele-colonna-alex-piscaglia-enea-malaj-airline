package controller.interfaces;

/**
 * Pilot Controller interface.
 */
public interface PilotController {

    /**
     * Removes a pilot from the list of pilots.
     */
    void removePilot();

}