package controller.interfaces;

/**
 * Plane Controller interface.
 */
public interface PlaneController {

    /**
     * Removes a plane from the list of planes.
     */
    void removePlane();

}