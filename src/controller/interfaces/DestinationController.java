package controller.interfaces;

/**
 * Destination Controller interface.
 */
public interface DestinationController {

    /**
     * Removes a destination from the list of destinations.
     */
    void removeDestination();

}