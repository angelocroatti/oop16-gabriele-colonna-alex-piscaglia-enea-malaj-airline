package controller.interfaces;

/**
 * Staff Controller interface.
 */
public interface StaffController {

    /**
     * Removes the account of the current user.
     */
    void removeStaff();

}