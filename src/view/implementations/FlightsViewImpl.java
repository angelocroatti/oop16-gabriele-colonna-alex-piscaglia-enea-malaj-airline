package view.implementations;

public class FlightsViewImpl extends AddRemoveViewImpl {

	public FlightsViewImpl() {
		super();
		this.setFrameTitle("Men� Voli");
		this.setLabelTitle("Airline Flights");
		this.setLabelViewedListTitle("Voli Disponibili:");
	}
}
