package view.implementations;

public class BookingsViewImpl extends AddRemoveViewImpl {

	public BookingsViewImpl() {
		super();
		this.setFrameTitle("Men� Prenotazioni");
		this.setLabelTitle("Airline Bookings");
		this.setLabelViewedListTitle("Prenotazioni:");
	}
}
